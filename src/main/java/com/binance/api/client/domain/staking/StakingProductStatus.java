package com.binance.api.client.domain.staking;

public enum StakingProductStatus {
    HOLDING,
    REDEEMING
}
