package com.binance.api.client.domain.staking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StakingLeftPersonalQuota {
    private String leftPersonalQuota;

    public String getLeftPersonalQuota() {
        return leftPersonalQuota;
    }

    public void setLeftPersonalQuota(String leftPersonalQuota) {
        this.leftPersonalQuota = leftPersonalQuota;
    }
}
