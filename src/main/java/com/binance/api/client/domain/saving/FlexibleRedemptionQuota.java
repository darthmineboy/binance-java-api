package com.binance.api.client.domain.saving;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FlexibleRedemptionQuota {
    private String productId;
    private String dailyQuota;
    private String leftQuota;
    private String minRedemptionAmount;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getDailyQuota() {
        return dailyQuota;
    }

    public void setDailyQuota(String dailyQuota) {
        this.dailyQuota = dailyQuota;
    }

    public String getLeftQuota() {
        return leftQuota;
    }

    public void setLeftQuota(String leftQuota) {
        this.leftQuota = leftQuota;
    }

    public String getMinRedemptionAmount() {
        return minRedemptionAmount;
    }

    public void setMinRedemptionAmount(String minRedemptionAmount) {
        this.minRedemptionAmount = minRedemptionAmount;
    }
}
