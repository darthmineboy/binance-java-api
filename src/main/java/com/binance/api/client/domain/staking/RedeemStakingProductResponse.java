package com.binance.api.client.domain.staking;

public class RedeemStakingProductResponse {
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
