package com.binance.api.client.domain.staking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StakingProduct {
    private String projectId;
    private StakingProductDetail detail;
    private StakingProductQuota quota;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public StakingProductDetail getDetail() {
        return detail;
    }

    public void setDetail(StakingProductDetail detail) {
        this.detail = detail;
    }

    public StakingProductQuota getQuota() {
        return quota;
    }

    public void setQuota(StakingProductQuota quota) {
        this.quota = quota;
    }
}
