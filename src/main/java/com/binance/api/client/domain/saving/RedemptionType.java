package com.binance.api.client.domain.saving;

public enum RedemptionType {
    /**
     * Fast redemption. You don't receive today's interest.
     */
    FAST,
    /**
     * Normal redemption. You do receive today's interest, but you have to wait longer for the product to be redeemed.
     */
    NORMAL
}