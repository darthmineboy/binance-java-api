package com.binance.api.client.domain.staking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StakingProductQuota {
    private String totalPersonalQuota;
    private String minimum;

    public String getTotalPersonalQuota() {
        return totalPersonalQuota;
    }

    public void setTotalPersonalQuota(String totalPersonalQuota) {
        this.totalPersonalQuota = totalPersonalQuota;
    }

    public String getMinimum() {
        return minimum;
    }

    public void setMinimum(String minimum) {
        this.minimum = minimum;
    }
}
