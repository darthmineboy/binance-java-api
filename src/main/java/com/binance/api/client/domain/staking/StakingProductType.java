package com.binance.api.client.domain.staking;

public enum StakingProductType {
    STAKING,
    /**
     * Flexible defi.
     */
    F_DEFI,
    /**
     * Locked defi.
     */
    L_DEFI
}
