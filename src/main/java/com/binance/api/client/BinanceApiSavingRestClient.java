package com.binance.api.client;

import com.binance.api.client.domain.saving.Featured;
import com.binance.api.client.domain.saving.FlexibleProduct;
import com.binance.api.client.domain.saving.LendingProductStatus;
import com.binance.api.client.domain.saving.PurchaseProductResponse;

import java.util.List;

public interface BinanceApiSavingRestClient {

    List<FlexibleProduct> getFlexibleProductList(
            LendingProductStatus status,
            Featured featured,
            int page,
            int size
    );

    PurchaseProductResponse purchaseFlexibleProduct(
            String productId,
            String amount
    );

}