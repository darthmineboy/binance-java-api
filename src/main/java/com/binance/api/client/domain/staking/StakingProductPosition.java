package com.binance.api.client.domain.staking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StakingProductPosition {
    private String positionId;
    private String projectId;
    private String asset;
    private String amount;
    private String purchaseTime;
    private String duration;
    private String accrualDays;
    private String rewardAsset;
    @JsonProperty("APY")
    private String apy;
    @JsonProperty("rewardAmt")
    private String rewardAmount;
    private String extraRewardAsset;
    private String extraRewardAPY;
    @JsonProperty("estExtraRewardAmt")
    private String estimateExtraRewardAmount;
    private String nextInterestPay;
    private String nextInterestPayDate;
    private String payInterestPeriod;
    private String redeemAmountEarly;
    private String interestEndDate;
    private String deliveryDate;
    private String redeemPeriod;
    @JsonProperty("redeemingAmt")
    private String redeemingAmount;
    private String partialAmountDeliveryDate;
    private boolean canRedeemEarly;
    private boolean renewable;
    private StakingProductPositionType type;
    private StakingProductStatus status;

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getAccrualDays() {
        return accrualDays;
    }

    public void setAccrualDays(String accrualDays) {
        this.accrualDays = accrualDays;
    }

    public String getRewardAsset() {
        return rewardAsset;
    }

    public void setRewardAsset(String rewardAsset) {
        this.rewardAsset = rewardAsset;
    }

    public String getApy() {
        return apy;
    }

    public void setApy(String apy) {
        this.apy = apy;
    }

    public String getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(String rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public String getExtraRewardAsset() {
        return extraRewardAsset;
    }

    public void setExtraRewardAsset(String extraRewardAsset) {
        this.extraRewardAsset = extraRewardAsset;
    }

    public String getExtraRewardAPY() {
        return extraRewardAPY;
    }

    public void setExtraRewardAPY(String extraRewardAPY) {
        this.extraRewardAPY = extraRewardAPY;
    }

    public String getEstimateExtraRewardAmount() {
        return estimateExtraRewardAmount;
    }

    public void setEstimateExtraRewardAmount(String estimateExtraRewardAmount) {
        this.estimateExtraRewardAmount = estimateExtraRewardAmount;
    }

    public String getNextInterestPay() {
        return nextInterestPay;
    }

    public void setNextInterestPay(String nextInterestPay) {
        this.nextInterestPay = nextInterestPay;
    }

    public String getNextInterestPayDate() {
        return nextInterestPayDate;
    }

    public void setNextInterestPayDate(String nextInterestPayDate) {
        this.nextInterestPayDate = nextInterestPayDate;
    }

    public String getPayInterestPeriod() {
        return payInterestPeriod;
    }

    public void setPayInterestPeriod(String payInterestPeriod) {
        this.payInterestPeriod = payInterestPeriod;
    }

    public String getRedeemAmountEarly() {
        return redeemAmountEarly;
    }

    public void setRedeemAmountEarly(String redeemAmountEarly) {
        this.redeemAmountEarly = redeemAmountEarly;
    }

    public String getInterestEndDate() {
        return interestEndDate;
    }

    public void setInterestEndDate(String interestEndDate) {
        this.interestEndDate = interestEndDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getRedeemPeriod() {
        return redeemPeriod;
    }

    public void setRedeemPeriod(String redeemPeriod) {
        this.redeemPeriod = redeemPeriod;
    }

    public String getRedeemingAmount() {
        return redeemingAmount;
    }

    public void setRedeemingAmount(String redeemingAmount) {
        this.redeemingAmount = redeemingAmount;
    }

    public String getPartialAmountDeliveryDate() {
        return partialAmountDeliveryDate;
    }

    public void setPartialAmountDeliveryDate(String partialAmountDeliveryDate) {
        this.partialAmountDeliveryDate = partialAmountDeliveryDate;
    }

    public boolean isCanRedeemEarly() {
        return canRedeemEarly;
    }

    public void setCanRedeemEarly(boolean canRedeemEarly) {
        this.canRedeemEarly = canRedeemEarly;
    }

    public boolean isRenewable() {
        return renewable;
    }

    public void setRenewable(boolean renewable) {
        this.renewable = renewable;
    }

    public StakingProductPositionType getType() {
        return type;
    }

    public void setType(StakingProductPositionType type) {
        this.type = type;
    }

    public StakingProductStatus getStatus() {
        return status;
    }

    public void setStatus(StakingProductStatus status) {
        this.status = status;
    }
}
