package com.binance.api.client.domain.staking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StakingProductDetail {
    private String asset;
    private String rewardAsset;
    private int duration;
    private boolean renewable;
    private String apy;

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getRewardAsset() {
        return rewardAsset;
    }

    public void setRewardAsset(String rewardAsset) {
        this.rewardAsset = rewardAsset;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isRenewable() {
        return renewable;
    }

    public void setRenewable(boolean renewable) {
        this.renewable = renewable;
    }

    public String getApy() {
        return apy;
    }

    public void setApy(String apy) {
        this.apy = apy;
    }
}
