package com.binance.api.client.domain.staking;

public enum StakingProductPositionType {
    AUTO,
    NORMAL
}
