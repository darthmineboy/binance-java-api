package com.binance.api.client.domain.saving;

public enum LendingProjectType {
    ACTIVITY,
    CUSTOMIZED_FIXED
}
