package com.binance.api.client.impl;

import com.binance.api.client.BinanceApiSavingRestClient;
import com.binance.api.client.constant.BinanceApiConstants;
import com.binance.api.client.domain.saving.Featured;
import com.binance.api.client.domain.saving.FlexibleProduct;
import com.binance.api.client.domain.saving.LendingProductStatus;
import com.binance.api.client.domain.saving.PurchaseProductResponse;

import java.util.List;

import static com.binance.api.client.impl.BinanceApiServiceGenerator.createService;
import static com.binance.api.client.impl.BinanceApiServiceGenerator.executeSync;

public class BinanceApiSavingRestClientImpl implements BinanceApiSavingRestClient {

    private final BinanceApiService binanceApiService;

    public BinanceApiSavingRestClientImpl(String apiKey, String secret) {
        binanceApiService = createService(BinanceApiService.class, apiKey, secret);
    }

    @Override
    public List<FlexibleProduct> getFlexibleProductList(LendingProductStatus status,
                                                        Featured featured,
                                                        int page,
                                                        int size) {
        return executeSync(binanceApiService.getFlexibleProducts(status, featured, page, size, BinanceApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis()));
    }

    @Override
    public PurchaseProductResponse purchaseFlexibleProduct(String productId, String amount) {
        return executeSync(binanceApiService.purchaseFlexibleProduct(productId, amount, BinanceApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis()));
    }

}