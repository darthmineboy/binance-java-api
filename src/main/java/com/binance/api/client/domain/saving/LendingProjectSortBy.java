package com.binance.api.client.domain.saving;

public enum LendingProjectSortBy {
    START_TIME,
    LOT_SIZE,
    INTEREST_RATE,
    DURATION
}
