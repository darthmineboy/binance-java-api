package com.binance.api.client.domain.saving;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum LendingProductStatus {
    ALL,
    /**
     * Product can be purchased.
     */
    PURCHASING,
    PURCHASED,
    /**
     * Product listed in APY, but not yet purchasable?
     */
    PREHEATING,
    /**
     * Activity ended.
     */
    REDEEMED,
    /**
     * Sold out.
     */
    RUNNING,
    PRE_REDEMPTION,
    END
}