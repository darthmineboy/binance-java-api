package com.binance.api.client;

import com.binance.api.client.domain.saving.Featured;
import com.binance.api.client.domain.staking.PurchaseStakingProductResponse;
import com.binance.api.client.domain.staking.StakingLeftPersonalQuota;
import com.binance.api.client.domain.staking.StakingProduct;
import com.binance.api.client.domain.staking.StakingProductType;

import java.util.List;

public interface BinanceApiStakingRestClient {

    List<StakingProduct> getStakingProductList(
            StakingProductType type,
            Featured featured,
            int page,
            int size
    );

    PurchaseStakingProductResponse purchaseStakingProduct(
            StakingProductType type,
            String productId,
            String amount,
            boolean renewable
    );

    /**
     * Set auto staking on locked staking or locked defi staking.
     *
     * @param type
     * @param positionId
     * @param renewable
     * @return true when successful, else false
     */
    boolean setAutoStaking(StakingProductType type, String positionId, boolean renewable);

    List<StakingLeftPersonalQuota> getPersonalLeftQuota(StakingProductType type, String productId);

}