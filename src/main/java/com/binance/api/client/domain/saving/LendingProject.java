package com.binance.api.client.domain.saving;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Fixed and activity project.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LendingProject {
    private String asset;
    private int displayPriority;
    private int duration;
    private String interestPerLot;
    private String interestRate;
    private String lotSize;
    private long lotsLowLimit;
    private long lotsPurchased;
    private long lotsUpLimit;
    private long maxLotsPerUser;
    private boolean needKyc;
    private String projectId;
    private String projectName;
    private LendingProductStatus status;
    private LendingProjectType type;
    private boolean withAreaLimitation;

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public int getDisplayPriority() {
        return displayPriority;
    }

    public void setDisplayPriority(int displayPriority) {
        this.displayPriority = displayPriority;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getInterestPerLot() {
        return interestPerLot;
    }

    public void setInterestPerLot(String interestPerLot) {
        this.interestPerLot = interestPerLot;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getLotSize() {
        return lotSize;
    }

    public void setLotSize(String lotSize) {
        this.lotSize = lotSize;
    }

    public long getLotsLowLimit() {
        return lotsLowLimit;
    }

    public void setLotsLowLimit(long lotsLowLimit) {
        this.lotsLowLimit = lotsLowLimit;
    }

    public long getLotsPurchased() {
        return lotsPurchased;
    }

    public void setLotsPurchased(long lotsPurchased) {
        this.lotsPurchased = lotsPurchased;
    }

    public long getLotsUpLimit() {
        return lotsUpLimit;
    }

    public void setLotsUpLimit(long lotsUpLimit) {
        this.lotsUpLimit = lotsUpLimit;
    }

    public long getMaxLotsPerUser() {
        return maxLotsPerUser;
    }

    public void setMaxLotsPerUser(long maxLotsPerUser) {
        this.maxLotsPerUser = maxLotsPerUser;
    }

    public boolean isNeedKyc() {
        return needKyc;
    }

    public void setNeedKyc(boolean needKyc) {
        this.needKyc = needKyc;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public LendingProductStatus getStatus() {
        return status;
    }

    public void setStatus(LendingProductStatus status) {
        this.status = status;
    }

    public LendingProjectType getType() {
        return type;
    }

    public void setType(LendingProjectType type) {
        this.type = type;
    }

    public boolean isWithAreaLimitation() {
        return withAreaLimitation;
    }

    public void setWithAreaLimitation(boolean withAreaLimitation) {
        this.withAreaLimitation = withAreaLimitation;
    }

    @Override
    public String toString() {
        return "LendingProject{" +
                "asset='" + asset + '\'' +
                ", displayPriority=" + displayPriority +
                ", duration=" + duration +
                ", interestPerLot='" + interestPerLot + '\'' +
                ", interestRate='" + interestRate + '\'' +
                ", lotSize='" + lotSize + '\'' +
                ", lotsLowLimit=" + lotsLowLimit +
                ", lotsPurchased=" + lotsPurchased +
                ", lotsUpLimit=" + lotsUpLimit +
                ", maxLotsPerUser=" + maxLotsPerUser +
                ", needKyc=" + needKyc +
                ", projectId='" + projectId + '\'' +
                ", projectName='" + projectName + '\'' +
                ", status=" + status +
                ", type=" + type +
                ", withAreaLimitation=" + withAreaLimitation +
                '}';
    }
}
