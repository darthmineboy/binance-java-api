package com.binance.api.client.domain.saving;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum ProductFilterStatus {
    ALL,
    SUBSCRIBABLE,
    UNSUBSCRIBABLE
}